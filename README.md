### Hi there, I'm George Nwankwo <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">

<!--
**BuiltbyGit/BuiltbyGit** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile. -->

This is the place where I opensource stuff and break things 🤣

- 🔭 I’m currently working on something really cool 😉
- 🌱 I’m currently learning everything 🤣
- 📫 How to reach me: [@GeorgieNwankwo][twitter] or say 👋 on Email at [Georgie Nwankwo](mailto:georgienwankwo@gmail.com)
- ⚡ Fun fact: I ❤️ 🐶s & ❤️ 🏍️

<br />

### Languages and Tools:
<img align="left" alt="JavaScript" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png" />
<img align="left" alt="React" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/react/react.png" />
<img align="left" alt="Python" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png" />
<img align="left" alt="Django" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/django/django.png" />
<img align="left" alt="PHP" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/php/php.png" />
<img align="left" alt="PostgreSQL" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/postgresql/postgresql.png" />
<img align="left" alt="Git" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png" />
<img align="left" alt="GitHub" width="26px" src="https://raw.githubusercontent.com/github/explore/78df643247d429f6cc873026c0622819ad797942/topics/github/github.png" />
<img align="left" alt="Terminal" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/terminal/terminal.png" />
<img align="left" alt="Node.js" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/nodejs/nodejs.png" />
<img align="left" alt="Visual Studio Code" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />
<img align="left" alt="AWS" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/aws/aws.png" />

<br />
<br />

### 📊 GitHub Stats:
![Georgie Nwankwo's github stats](https://github-readme-stats.vercel.app/api?username=BuiltbyGit&show_icons=true&theme=dracula&count_private=true&include_all_commits=true&hide=contribs,issues,stars)


[instagram]: https://www.instagram.com/GeorgieNwankwo/
[twitter]: https://twitter.com/GeorgieNwankwo
[facebook]: https://www.facebook.com/vibhorchaudhary
